<?php
            $servername = 'localhost';
            $username = 'root';
            $password = 'root';
            
            try{
                $conn = new PDO("mysql:host=$servername;dbname=Citation.fr", $username, $password);
                //On définit le mode d'erreur de PDO sur Exception
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            

            catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
            }
        ?>