<?php 
    require("connexion.php")
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <! Google fonts -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet"> 
    <title>Ajouter</title>
</head>
<body>
    <?php include("header.php") ?>
    <main class="#">
        <div class="container">
            <h2>Ajouter une citation:</h2>
            <div class="formAdd">
                <form method="post">
                    <label>Rentrez une citation</label>
                    <input type="text" placeholder="Citation" name="citation">
                    <label>Choisir un auteur, ou rentrez un nouvel auteur:</label>
                    <div class="chooseAutor">
                        <?php 
                        $auteur = "SELECT * FROM auteurs";
                        $resultat = $conn-> query($auteur);
                        ?>
                        <select name="auteur">
                        <option value="0">-- Chosir un auteur --</option>
                        <?php
                        while ($ligne = $resultat->fetch(PDO::FETCH_ASSOC)) { ?>
                                <option value="<?php echo $ligne['id']; ?>"><?php echo $ligne['Nom']; ?> <?php echo $ligne['Prenom']; ?></option>
                        <?php } ?>
                        </select>
                        <input type="text" placeholder="Nom" name="nom">
                        <input type="text" placeholder="Prenom" name="prenom">
                        <input class="goCit" type="submit" name="ajouter">
                    </div>
                </form>
                <?php
                    if (!empty($_POST["ajouter"])) {
                        $requete = "SELECT * FROM auteurs JOIN citations ON  auteurs.id = citations.Id_auteurs ";
                        $resultat = $conn -> query($requete);
                        while ($ligne = $resultat->fetch(PDO::FETCH_ASSOC)) {
                            $idAutor = $ligne['id'];
                        }
                        $citation = $_POST['citation'];
                        $auteur = $_POST['auteur'];
                        $nom = $_POST['nom'];
                        $prenom = $_POST['prenom'];
                        if ( $auteur == "0" ) {
                            $statement = $conn->prepare("INSERT INTO citations (`Description`, Id_auteurs) VALUES(:descri, :id_auteurs)"); 
                            $statement2 = $conn->prepare("INSERT INTO auteurs (Nom, Prenom) VALUES(:nom, :prenom)");
                            $statement->bindParam(':descri', $citation, PDO::PARAM_STR,255);
                            $statement->bindParam(':id_auteurs', $idAutor , PDO::PARAM_INT);
                            $statement2->bindParam(':nom', $nom, PDO::PARAM_STR,255 );
                            $statement2->bindParam(':prenom', $prenom, PDO::PARAM_STR,255);
                            $statement->execute();
                            $statement2->execute();
                        }else{
                            $statement = $conn->prepare("INSERT INTO citations (`Description`, Id_auteurs) VALUES(:descri, :id)"); 
                            $statement->bindParam(':descri', $citation, PDO::PARAM_STR,255);
                            $statement->bindParam(':id', $auteur, PDO::PARAM_INT);
                            $statement->execute();
                        }
                        
                        }
                ?>
            </div>
        </div>
    </main>
    <?php include ("footer.php") ?>
</body>
</html>