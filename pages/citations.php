<?php 
    require("connexion.php")
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">

    <! Google fonts -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet"> 
    <title>Citations</title>
</head>
<body>
    <?php include("header.php") ?>
    <main class="citations">
        <h2>Les citations:</h2>
        <div class="container" id="coucou">
            <div class="add">
                <a href="ajouter.php"><button class="ajouter">Ajouter une citation</button></a>
            </div>
        <?php 
        $requete = "SELECT * FROM citations JOIN auteurs ON citations.Id_auteurs = auteurs.id; ";
            $resultat = $conn -> query($requete);
            while ($ligne = $resultat->fetch(PDO::FETCH_ASSOC)) { ?>
                <div class="allCitations">
                    <p>"<?php echo $ligne['Description'] ?>"</p>
                    <div id="modif">
                        <div class="option">
                            <p><a href="edit.php?ID=<?php echo $ligne['Id']?>">Modifier</a><p>
                            <p><a href="delete.php?ID=<?php echo $ligne['Id']?>"onclick="return confirm('Etes vous sur de vouloir supprimer ?')">Supprimer</a><p>
                        </div>
                        <h6><?php echo $ligne['Nom'] ?> <?php echo $ligne['Prenom'] ?></h6>
                    </div>
                </div>
            <?php } ?>
            </div>
    </main>
    <?php include ("footer.php") ?>
</body>
</html>