<header>
    <div class="logoTitle">
        <img class="logo" src="../img/logo_citation.png" alt="Logo citation">
        <h1 class="titre"><a href="index.php">Citation.fr</a></h1>
    </div>    
    <div>
        <ul class="navigation">
        <a href="homepage.php"><li>Homepage</li></a>
        <a href="citations.php"><li>Les citations</li></a>
        <a href="auteurs.php"><li>Les auteurs</li></a>
        <a href="citations_auteurs.php"><li>Les citations d'un auteur</li></a>
        </ul>
    </div>
</header>
    